import React from 'react';
import LoginScreen from './Screens/LoginScreen';
import LoadingScreen from './Screens/LoadingScreen';
import RegisterScreen from './Screens/RegisterScreen';
import HomeScreen from './Screens/HomeScreen';
import SplashScreen from './Screens/SplashScreen';
import Posts from './Screens/Posts';
import {createAppContainer , createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import CreatePost from './Screens/CreateNewPost';
import * as firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyCaIN1NuC8bRTPdil-mxrnLspqysP7a6aw",
  authDomain: "twitter-replica-5ac4c.firebaseapp.com",
  databaseURL: "https://twitter-replica-5ac4c.firebaseio.com",
  projectId: "twitter-replica-5ac4c",
  storageBucket: "twitter-replica-5ac4c.appspot.com",
  messagingSenderId: "235048337599",
  appId: "1:235048337599:web:884eb5cfa7c57b2787060a"
};
if(!firebase.apps.length){
firebase.initializeApp(firebaseConfig);
}

const AppStack = createStackNavigator({
  Home : HomeScreen,
  CreatePost : CreatePost
})

const AuthStack = createStackNavigator({
  Splash : SplashScreen,
  Login : LoginScreen,
  Register : RegisterScreen
})

export default createAppContainer(
  createSwitchNavigator(
    {
      Loading : LoadingScreen,
      App : AppStack,
      Auth : AuthStack
    },
    {
      initialRouteName : 'Loading'
    }
  )
)
