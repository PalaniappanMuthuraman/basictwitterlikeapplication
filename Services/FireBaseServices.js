import * as firebase from 'firebase';
import { Observable } from 'rxjs';

export const getPosts = () => {
    const userID = firebase.auth().currentUser.uid;
    var databaseRef = firebase.database().ref("posts/" +userID);

    //By Using Promise

    // return new Promise(function(reject,resolve){
    //     // firebase.database().ref("posts/" + userID).on('value',function(data){
    //     //     const fireBaseObject = data.val();
    //     //     return resolve(fireBaseObject);
    //     // })
    //     databaseRef.once('value').then(result => {
    //         console.log("Result is",result);
    //         return resolve(result)
    //     }).catch(error => {
    //         return reject(error)
    //     })
    // })

    //By using normal Then and catch block

    // databaseRef.once('value').then(result => {
    //     return result;
    // }).catch(error => {
    //     return error;
    // })
    // return new Observable((observer) => {
    //     databaseRef.once('value').then(
    //         result => {
    //             observer.next(result);
    //             observer.complete();
    //         }
    //     )
    //     .catch(error => {
    //         observer.error(error);
    //         observer.complete();
    //     })
    // })

    return new Observable((observer) => {
        databaseRef.on('value',function(data){
            observer.next(data.val());
            observer.complete();
        })
    })

    
}

export const postTweet = (post) => {
    const userID = firebase.auth().currentUser.uid;
    var databaseRef = firebase.database().ref("posts/" +userID);
    return new Observable((observer) => {
        databaseRef.push({
            post : post,
            time : Date.now()
        }).then(result => {
            observer.next('Post Uploaded');
            observer.complete();
        }).catch(error => {
            observer.error(error);
            observer.complete();
        })
    })
}


export const followingUserForCurrentUSer = () => {
    const userId = firebase.auth().currentUser.uid;
    var databaseRef = firebase.database().ref("following/");
    return new Observable((observer) => {
        databaseRef.child(userId).on('value',function(data){
            const valuesFromDb = data.val();
            if(valuesFromDb != null){
            const valueFromObject = Object.values(valuesFromDb);
            observer.next(valueFromObject);
            observer.complete();
            }
            else{
                observer.complete();
            }
        })
    })
}

export const isUserFollowingThisUser = (followerUSerId) => {
    console.log("Follower USer Id" , followerUSerId);
    const userId = firebase.auth().currentUser.uid;
    var databaseRef = firebase.database().ref("following/");
    // return new Observable ((observer) => {
    //     databaseRef.child(userId).on('value' , function(data){
    //         const valuesFromDb = data.val();
    //         const valueFromObject = Object.values(valuesFromDb);
    //         valueFromObject.forEach(
    //             followingUserId => {
    //                 if(followerUSerId === followingUserId){
    //                     observer.next(true);
    //                     observer.complete();
    //                 }
    //             }
    //         )
    //         observer.next(false);
    //         observer.complete();
    //     })
    // })
}


export const followUser = (followingUserID) =>{
    const userId = firebase.auth().currentUser.uid;
    var databaseRef = firebase.database().ref("following/");
    return new Observable ((observer) => {
        databaseRef.child(userId).push(followingUserID).then(
            result => {
                observer.next("Follow Success");
                observer.complete();
            }
        ).catch(error => {
            alert("Error");
            observer.error(error);
            observer.complete();
        });
    })
}