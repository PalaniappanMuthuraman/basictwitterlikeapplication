import React , {useEffect,useState} from 'react';
import {Text,View,TouchableOpacity,StyleSheet,TextInput} from 'react-native';
import {postTweet, getPosts} from '../Services/FireBaseServices'
import Toast from 'react-native-simple-toast';

const postTweetUI = (post,navigation) => {
  if(post !== ""){
    postTweet(post).subscribe(
        result => {
            navigation.navigate('Home');
            Toast.show(result);
        } , error => {
          alert(error);
        }
    )
  }
  else{
    Toast.show("Please Typ Some Text");
  }
}
const Index = ({navigation}) => {
    const [post, setpost] = useState("");
    return(
        <View style={styles.container}>
      <View style = {styles.form}>
        <View style = {{marginTop : 32}}>
            <Text style = {styles.inputTitle}>Type Here</Text>
            <TextInput style = {styles.input} autoCapitalize = "none" onChangeText = {(post) => setpost(post)} value = {post}></TextInput>
        </View>
      </View>

      <TouchableOpacity style = {styles.button} onPress = {() => postTweetUI(post,navigation)}>
        <Text style = {{color : "#FFF", fontWeight : "500"}}>Post</Text>
      </TouchableOpacity>
     </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    createAccount: {
      fontSize: 18,
      textAlign: "center",
      marginTop: 20
    },
    error : {
        color : "#E9446A",
        fontSize : 13,
        fontWeight : "600",
        textAlign : "center"
    },
    secondContent: {
      fontSize: 18,
      marginStart: 10,
      fontWeight : "400",
      marginTop: 15,
      textAlign: "left"
    },
    errorMessage : {
        height : 72,
        alignItems : "center",
        justifyContent : "center",
        marginHorizontal : 30
    },
    form : {
       marginBottom : 48,
       marginHorizontal : 30
    },
    inputTitle : {
        color : "#8A8F9E",
        fontSize : 10,
        textTransform : "uppercase"
    },
    input : {
        borderBottomColor : "#8A8F9E",
        borderBottomWidth : StyleSheet.hairlineWidth,
        height : 40,
        fontSize : 15,
        color : "#161F3D"
    },
    button : {
        marginHorizontal : 30,
        backgroundColor : "#E9446A",
        borderRadius : 4,
        height : 52,
        alignItems : "center",
        justifyContent : "center"
    }
  });

export default Index;