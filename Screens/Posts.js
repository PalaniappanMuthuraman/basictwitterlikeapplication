import React, { useEffect, useState } from "react";
import { Text, FlatList ,StyleSheet,View,SafeAreaView,TouchableOpacity} from "react-native";
import { getPosts } from "../Services/FireBaseServices";
import { FAB    } from 'react-native-paper';

import * as firebase from 'firebase';

const Index = ({navigation}) => { 
  const [posts,setPosts] = useState([]);
  useEffect(() => {
    console.log("USe effect in myposts");
    getPosts().subscribe((res,err) => {
      if(res!==null){
      setPosts(Object.values(res));
      }
      if(err){
        alert(err);
      }
    });
    return () => {
    };
  });
  return (
    <View style={styles.container}>
    <FlatList data = {posts}
    renderItem = {({item}) => <Item post = {item.post} time = {(item.time)}></Item>}
    keyExtractor={item => item.id}/>
    <FAB
    style={styles.fab}
    small
    icon="plus"
    onPress={() => navigation.navigate('CreatePost')}
  />
    </View>
  )
};

function Item({ post ,time}) {
    const timeTaken = Date.now() - time;
    const minutes = Math.floor(timeTaken / 1000);
    const seconds = Math.floor(minutes/60);
    return (
      <View style={styles.item}>
        <Text>{post}</Text>
        <Text>{seconds} mins ago</Text>
      </View>
    );
  }



export default Index;


const styles = StyleSheet.create({
  container : {
      flex : 1
  },
  item: {
      backgroundColor: '#f9c2ff',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
    },
    fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0
  }
})