import React from "react";
import { View,StyleSheet, SafeAreaView, TouchableOpacity ,Text} from "react-native";
import MyProfile from "./MyProfile";
import UserList from "./UserList";
import { createDrawerNavigator, DrawerItems } from "react-navigation-drawer";
import { createAppContainer } from "react-navigation";
import { FAB    } from 'react-native-paper';
import * as firebase from 'firebase';
import Posts from './Posts';
const signOut = () => {
     return firebase.auth().signOut()
}

const Index = ({navigation}) => {
  const AppNavigator = createAppContainer(AppDrawerNavigator);
  return (
    <View style={styles.container}>
      <AppNavigator />
      <FAB
    style={styles.fab}
    small
    icon="plus"
    onPress={() => navigation.navigate('CreatePost')}
  />
    </View>
  );
};

const AppDrawerNavigator = createDrawerNavigator(
  {
    MyPosts : Posts,
    MyProfile: MyProfile,
    UserList: UserList,
    SignOut : signOut
  }
);

const customDrawerComponent = props => {
  <SafeAreaView style={{ flex: 1 }}>
    {/* <View style = {{height : 150 , backgroundColor : 'white'}}>
        <Image style = {{height : 120 , width : 120,borderRadius : 60}}/>
    </View> */}
    <DrawerItems {...props} />
  </SafeAreaView>;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  text: {
    color: "#161924",
    fontSize: 20,
    fontWeight: "500"
  },
  button : {
    marginHorizontal : 30,
    backgroundColor : "#E9446A",
    borderRadius : 4,
    height : 52,
    alignItems : "center",
    justifyContent : "center"
},
fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0
  },
});

export default Index;
