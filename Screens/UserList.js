import React , {useEffect,useState} from "react";
import { View, Text, StyleSheet,FlatList,SafeAreaView} from "react-native";
import * as firebase from 'firebase';
import CustomRow from './CustomRow'
import {followingUserForCurrentUSer} from '../Services/FireBaseServices';



const Index = () => {
    const [userList, setuserList] = useState([]);
    const [followingList, setfollowingList] = useState([]);
    const newArr = [];
    useEffect(() => {
        const subscriber = firebase.database().ref('users/').on('value',function(data){
            const firebsaseObject = data.val();
            if(firebsaseObject != null){
            Object.values(firebsaseObject).forEach(
              data =>{
                  if(data.id !== firebase.auth().currentUser.uid){
                    newArr.push(data);
                  }
              }
            )
            setuserList(newArr);
            }
        })
        const followSubscriber = followingUserForCurrentUSer().subscribe(
          res => {
            setfollowingList(res);
          }
        )

        
        return () => {
            subscriber()
            followSubscriber()
        }
    }, [0])

    function Item({ title }) {
        return (
          <View style={styles.item}>
            <Text style={styles.title}>{title}</Text>
          </View>
        );
      }

  return (
    //   <Users users = {newArr}></Users>
    <SafeAreaView style={styles.container}>
      <FlatList
        data={userList}
        renderItem={({ item }) => <CustomRow name={item.name} isFollowing = {followingList.includes(item.id)} userId = {item.id}/>}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
    
    // <Text>Hiii</Text>
  )
};

const styles = StyleSheet.create({
    container : {
        flex : 1
    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
      }
})

export default Index;
