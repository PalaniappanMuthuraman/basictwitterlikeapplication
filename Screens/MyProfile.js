import React from "react";
import {createMaterialTopTabNavigator} from "@react-navigation/material-top-tabs";
import Followers from './Followers';
import Following from './Following';
import OtherPosts from './OthersPosts';
import { NavigationContainer } from '@react-navigation/native';
import {StyleSheet} from 'react-native';

const Tab = createMaterialTopTabNavigator();

const Index = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Posts" component={OtherPosts} />
        <Tab.Screen name="Followers" component={Followers} />
        <Tab.Screen name="Following" component={Following} />        
      </Tab.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
})

export default Index;
