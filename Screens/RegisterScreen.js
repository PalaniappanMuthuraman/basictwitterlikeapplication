import React , {useState} from "react";
import { View, Text, StyleSheet , TextInput , TouchableOpacity } from "react-native";
import * as firebase from 'firebase';

const Index = () => {
    const [email,setEmail] = useState("");
    const [password, setpassword] = useState("");
    const [confirmPassword,setconfirmPassword] = useState("");
    const [errorMessageFromFireBase, setErrorMessageFromFireBase] = useState(null);
    const [name, setname] = useState("")

    const handleSignUp = () => {
        if(password === confirmPassword){
            firebase.auth().createUserWithEmailAndPassword(email,password).then(
                userCredentials => {
                    firebase.database().ref('users/').push({
                        id : userCredentials.user.uid,
                        name : name,
                        email : email
                    }).then(() => {
                        return userCredentials.user.updateProfile({
                            displayName : email
                        })
                    })
                }
            ).catch(error => {
                setErrorMessageFromFireBase(error.message);
            })
        }
        else{
            alert('Passwords does not match');
        }
    }
  return (
    <View style={styles.container}>
      <Text style={styles.createAccount}>Create Account</Text>
      <Text style={styles.secondContent}>{`Fill in the required details and click Proceed.\nFields marked * are mandatory`}</Text>
      <View style = {styles.errorMessage}>
        {
            errorMessageFromFireBase && <Text style = {styles.error}>{errorMessageFromFireBase}</Text>
        }
      </View>
      <View style = {styles.form}>
      <View>
            <Text style = {styles.inputTitle}>Name</Text>
            <TextInput style = {styles.input} autoCapitalize = "none" onChangeText = {(name) => setname(name)} value = {name}></TextInput>
        </View>
        <View style = {{marginTop : 32}}>
            <Text style = {styles.inputTitle}>Email Address</Text>
            <TextInput style = {styles.input} autoCapitalize = "none" onChangeText = {(email) => setEmail(email)} value = {email}></TextInput>
        </View>
        <View style = {{marginTop : 32}}>
            <Text style = {styles.inputTitle}>Password</Text>
            <TextInput style = {styles.input} secureTextEntry autoCapitalize = "none" onChangeText = {(password) => setpassword(password)} value = {password}></TextInput>
        </View>

        <View style = {{marginTop : 32}}>
            <Text style = {styles.inputTitle}>Confirm Password</Text>
            <TextInput style = {styles.input} secureTextEntry autoCapitalize = "none" onChangeText = {(password) => setconfirmPassword(password)} value = {confirmPassword}></TextInput>
        </View>
      </View>

      <TouchableOpacity style = {styles.button} onPress = {handleSignUp}>
        <Text style = {{color : "#FFF", fontWeight : "500"}}>Sign Up</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  createAccount: {
    fontSize: 18,
    textAlign: "center",
    marginTop: 20
  },
  error : {
      color : "#E9446A",
      fontSize : 13,
      fontWeight : "600",
      textAlign : "center"
  },
  secondContent: {
    fontSize: 18,
    marginStart: 10,
    fontWeight : "400",
    marginTop: 15,
    textAlign: "left"
  },
  errorMessage : {
      height : 72,
      alignItems : "center",
      justifyContent : "center",
      marginHorizontal : 30
  },
  form : {
     marginBottom : 48,
     marginHorizontal : 30
  },
  inputTitle : {
      color : "#8A8F9E",
      fontSize : 10,
      textTransform : "uppercase"
  },
  input : {
      borderBottomColor : "#8A8F9E",
      borderBottomWidth : StyleSheet.hairlineWidth,
      height : 40,
      fontSize : 15,
      color : "#161F3D"
  },
  button : {
      marginHorizontal : 30,
      backgroundColor : "#E9446A",
      borderRadius : 4,
      height : 52,
      alignItems : "center",
      justifyContent : "center"
  }
});

export default Index;
