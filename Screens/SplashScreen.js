import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import * as firebase from 'firebase';

const Index = ({navigation}) => {
    
    const navigateToLogin = () => {
        navigation.navigate("Login");
    }
  return <View style = {styles.container}>
      <Text style = {styles.textStyle}>TweetX is a social app that lets you share your moments with friends</Text>

      <TouchableOpacity style = {styles.button} onPress = {navigateToLogin}>
        <Text style = {{color : "#FFF", fontWeight : "500"}}>Sign In</Text>
      </TouchableOpacity>

      <TouchableOpacity style = {{alignSelf : "center" , marginTop : 32}} onPress = {() => navigation.navigate("Register")} >
        <Text style = {{fontWeight : "500" , color : "#E9446A" , marginTop : 40}}>Create New Account</Text>
      </TouchableOpacity>
  </View>;
};

const styles = StyleSheet.create({
    container : {
        flex : 1
    },
    textStyle : {
        marginTop:300,
        fontFamily : "Roboto",
        margin:50
    },
    button : {
        marginHorizontal : 30,
        backgroundColor : "#E9446A",
        borderRadius : 4,
        height : 52,
        alignItems : "center",
        justifyContent : "center"
    }
})

export default Index;
