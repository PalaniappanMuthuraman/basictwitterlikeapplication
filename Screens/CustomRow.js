import React from 'react';
import { View, Text, StyleSheet, Image , TouchableOpacity } from 'react-native';
import UserAvatar from 'react-native-user-avatar';
import {followUser} from '../Services/FireBaseServices';
import Toast from 'react-native-simple-toast';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        marginLeft:16,
        marginRight:16,
        marginTop: 8,
        marginBottom: 8,
        borderRadius: 5,
        backgroundColor: '#FFF',
        elevation: 2,
    },
    title: {
        fontSize: 16,
        color: '#000',
    },
    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    description: {
        fontSize: 11,
        fontStyle: 'italic',
    },
    photo: {
        height: 50,
        width: 50,
    },
    followStyle : {
        alignItems : "flex-end",
        justifyContent : "flex-end"
    }
});

const followThisUser = (followingUserId) => {
    followUser(followingUserId).subscribe(
        result => {
            Toast.show(result);
        }
    )
}

const CustomRow = ({ name , isFollowing , userId}) => (
    <View style={styles.container}>
     <UserAvatar size={50} name= {name}  style = {styles.photo}/>
        <View style={styles.container_text}>
            <Text style={styles.title}>
                {name}
            </Text>
            {
                (isFollowing && <Text>Following</Text>)
            }
            {
                (!isFollowing && <TouchableOpacity onPress = {() => followThisUser(userId)}><Text style = {styles.followStyle}>Follow</Text></TouchableOpacity>)
            }
        </View>
    </View>
);

export default CustomRow;